package ru.example.olegz.lifecycleexample;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends LifecycleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.location_text);

        LocationUIModel locationUIModel = ViewModelProviders.of(this).get(LocationUIModel.class);

        new LocationManagerListener(this, this.getLifecycle(), new LocationListenerCallback() {

            @Override
            public void onLocationChange(Location location) {
                locationUIModel.getLocationLiveData().setValue(location);
            }

            @Override
            public void onStart() {
                textView.append(getString(R.string.start_location_listen));
            }

            @Override
            public void onStop() {
                textView.append(getString(R.string.stop_location_listen));
            }
        });


        locationUIModel.getLocationLiveData().observe(this, location -> {
            String locationStr = getString(R.string.location_fmt, location.getLatitude(), location.getLongitude());
            textView.append(locationStr);
        });
    }
}
