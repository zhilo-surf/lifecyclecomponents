package ru.example.olegz.lifecycleexample;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationManagerListener implements LifecycleObserver {

    private static final String TAG = LocationManagerListener.class.getSimpleName();
    public static final int MIN_TIME = 1000 * 10;

    private Context context;
    private Lifecycle lifecycle;
    private LocationListenerCallback listener;
    private LocationManager locationManager;
    private LocationListener locationListener;

    public LocationManagerListener(Context context,
                                   Lifecycle lifecycle,
                                   LocationListenerCallback listener) {
        this.context = context;
        this.lifecycle = lifecycle;
        this.listener = listener;
        this.locationListener = getLocationListener();
        this.lifecycle.addObserver(this);

        locationManager = (LocationManager) context.getSystemService(
                Context.LOCATION_SERVICE);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        listener.onStart();
        //use pre 23 api
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, 0, locationListener);
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void pause() {
        locationManager.removeUpdates(locationListener);
        listener.onStop();
    }

    private LocationListener getLocationListener() {
        return new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                listener.onLocationChange(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //empty
            }

            @Override
            public void onProviderEnabled(String provider) {
                //empty
            }

            @Override
            public void onProviderDisabled(String provider) {
                //empty
            }
        };
    }
}