package ru.example.olegz.lifecycleexample;

import android.arch.lifecycle.ViewModel;


public class LocationUIModel extends ViewModel {

    private LocationLiveDate locationLiveData = new LocationLiveDate();

    public LocationLiveDate getLocationLiveData() {
        return locationLiveData;
    }
}