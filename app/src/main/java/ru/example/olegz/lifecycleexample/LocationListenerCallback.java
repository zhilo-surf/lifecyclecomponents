package ru.example.olegz.lifecycleexample;

import android.location.Location;

/**
 * Created by olegz on 12.06.2017.
 */

interface LocationListenerCallback {
    void onLocationChange(Location location);
    void onStart();
    void onStop();
}