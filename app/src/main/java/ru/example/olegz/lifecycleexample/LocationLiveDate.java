package ru.example.olegz.lifecycleexample;

import android.arch.lifecycle.LiveData;
import android.location.Location;
import android.support.annotation.Nullable;

public class LocationLiveDate extends LiveData<Location> {

    @Override
    protected void setValue(Location value) {
        super.setValue(value);
    }

    @Nullable
    @Override
    public Location getValue() {
        return super.getValue();
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }
}
